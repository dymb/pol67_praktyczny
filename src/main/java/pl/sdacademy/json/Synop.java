package pl.sdacademy.json;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Function;
import java.util.function.Predicate;

public class Synop {
    public static void main(String[] args) throws IOException {
        String daneUrl = "https://danepubliczne.imgw.pl/api/data/synop";

        ObjectMapper om = new ObjectMapper();

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Stacja[] stacje = om.readValue(new URL(daneUrl), Stacja[].class);

        Arrays.stream(stacje)
//                .sorted((s1, s2) -> s1.getStacja().compareToIgnoreCase(s2.getStacja()))
                .sorted((s1, s2) -> (int)(100 * (s1.getTemperatura() - s2.getTemperatura())))
                .forEach(s -> System.out.println(s.getStacja() + " " + s.getTemperatura()));


        Double srednia = sredniaTemperatura(stacje);
        Stacja maxTemp = maxTemperatura(stacje);

        System.out.println("Srednia: " + srednia);
        System.out.println("Najcieplej jest w " + maxTemp.getStacja() + " " + maxTemp.getTemperatura());
    }

    private static Double sredniaTemperatura(Stacja[] stacje) {
        Double sum = Arrays.stream(stacje)
                .map(stacja -> stacja.getTemperatura())
                .reduce(0.0, (acc, value) -> acc + value);
        return sum / stacje.length;
    }

    private static Stacja maxTemperatura(Stacja[] stacje) {
        return Arrays
                .stream(stacje)
                .filter(stacja -> stacja.getTemperatura() != null)
//                .max(Comparator.comparing(Stacja::getTemperatura)) //lub tak
                .max((o1, o2) -> (int)(o1.getTemperatura() - o2.getTemperatura()))
                .get();
    }
}
