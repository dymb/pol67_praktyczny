package pl.sdacademy.json;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/*
{
        "id_stacji":"12295",
        "stacja":"Bia\u0142ystok",
        "data_pomiaru":"2021-04-06",
        "godzina_pomiaru":"16",
        "temperatura":"3.3",
        "predkosc_wiatru":"2",
        "kierunek_wiatru":"250",
        "wilgotnosc_wzgledna":"54.7",
        "suma_opadu":"1.3",
        "cisnienie":"1002.8"
        }
  */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Stacja {
    @JsonProperty("id_stacji")
    private Integer id;
    private String stacja;
    private Double temperatura;

    @JsonProperty("data_pomiaru")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dataPomiaru;

    @JsonProperty("godzina_pomiaru")
    private Integer godzinaPomiaru;
    @JsonProperty("predkosc_wiatru")
    private Integer predkosc;
    @JsonProperty("kierunek_wiatru")
    private Integer kierunek;
    @JsonProperty("wilgotnosc_wzgledna")
    private Double wilgotnosc;
    @JsonProperty("suma_opadu")
    private Double opad;
    private Double cisnienie;

    public Integer getGodzinaPomiaru() {
        return godzinaPomiaru;
    }

    public void setGodzinaPomiaru(Integer godzinaPomiaru) {
        this.godzinaPomiaru = godzinaPomiaru;
    }

    public Integer getPredkosc() {
        return predkosc;
    }

    public void setPredkosc(Integer predkosc) {
        this.predkosc = predkosc;
    }

    public Integer getKierunek() {
        return kierunek;
    }

    public void setKierunek(Integer kierunek) {
        this.kierunek = kierunek;
    }

    public Double getWilgotnosc() {
        return wilgotnosc;
    }

    public void setWilgotnosc(Double wilgotnosc) {
        this.wilgotnosc = wilgotnosc;
    }

    public Double getOpad() {
        return opad;
    }

    public void setOpad(Double opad) {
        this.opad = opad;
    }

    public Double getCisnienie() {
        return cisnienie;
    }

    public void setCisnienie(Double cisnienie) {
        this.cisnienie = cisnienie;
    }

    public Date getDataPomiaru() {
        return dataPomiaru;
    }

    public void setDataPomiaru(Date dataPomiaru) {
        this.dataPomiaru = dataPomiaru;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStacja() {
        return stacja;
    }

    public void setStacja(String stacja) {
        this.stacja = stacja;
    }

    public Double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(Double temperatura) {
        this.temperatura = temperatura;
    }
}
